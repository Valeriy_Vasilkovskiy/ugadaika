package gameWindow;

import LoadingWindow.SettingsAndDecorView;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;


public class GameViewPanel implements IGameview {
    private static GameViewPanel instance;

    public static GameViewPanel getInstanceGame() {
        if (instance == null) {
            instance = new GameViewPanel();
        }
        return instance;
    }

    private IPresenterTogame iPresenterTogame;

    private ArrayList<JButton> buttonArray;

    private JPanel jPanelKeyBoard;
    private JPanel jPanelInfoAndInput;

    private ArrayList<String> stringArrayList = new ArrayList<>();
    private ArrayList<JButton> arrayAnsver = new ArrayList<>(stringArrayList.size());

    public JPanel getjPanelInfoAndInput() {
        return jPanelInfoAndInput;
    }

    public JPanel getjPanelKeyBoard() {
        return jPanelKeyBoard;
    }

    private int missTake = 12;
    private int size = 0;

    private GameViewPanel() {
        GridLayout markup = new GridLayout(4, 9, 30, 30);
        iPresenterTogame = new PresenterToGame(this);
        jPanelKeyBoard = new JPanel();
        Dimension dimension = new Dimension();
//        jPanelKeyBoard.setBackground(Color.GREEN);
        dimension.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                SettingsAndDecorView.getjFrameStartWindow().getHeight() / 2.2);
        jPanelKeyBoard.setPreferredSize(dimension);
//        jPanelKeyBoard.setLocation(SettingsAndDecorView.getjFrameStartWindow().getX(),SettingsAndDecorView.getjFrameStartWindow().getY() +300);
        jPanelKeyBoard.setLayout(markup);

        JButton jButtonExample = new JButton(" ");
        jButtonExample.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 20,
                SettingsAndDecorView.getjFrameStartWindow().getHeight() / 16);

        String[] arrayRus = {
                "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й",
                "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф",
                "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"
        };
        buttonArray = new ArrayList<>(arrayRus.length);

        for (int i = 0; i != 33; i++) {
            String name = arrayRus[i];
            buttonArray.add(new JButton());
            buttonArray.get(i).setName(name);
            buttonArray.get(i).setSize(jButtonExample.getWidth(), jButtonExample.getHeight());
            buttonArray.get(i).setText(arrayRus[i]);
            buttonArray.get(i).setFont(new Font("Helvetica", Font.BOLD, 18));
            markup.addLayoutComponent(buttonArray.get(i).getName(), buttonArray.get(i));
            jPanelKeyBoard.add(buttonArray.get(i));
        }
        JTextField jTextField = new JTextField();
        jTextField.setSize(100, 100);
        jTextField.setFont(new Font("Arial", Font.PLAIN, 18));
        jTextField.setEditable(false);
        jTextField.setText(String.valueOf(missTake));

        for (JButton a : buttonArray) {
            a.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //Если такая буква есть, то кнопка зелёная, если нет - красная
                    if (iPresenterTogame.NumberCheek(stringArrayList, e, a.getName())) {
                        for (JButton z : arrayAnsver) {
                            if (z.getName().equals(e.getActionCommand())) {
                                z.setText(z.getName());
                                z.setFont(new Font("Helvetica", Font.BOLD, 18));
                            }
                        }
                    } else {
                        if (missTake != 0) {
                            missTake -= 1;
                            jTextField.setText(String.valueOf(missTake));
                        } else {
                            JDialog jDialog = new JDialog();
                            jDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                            JPanel jPanelDialog = new JPanel();
                            jDialog.setSize(400, 400);
                            jTextField.setText("Вы проиграли");
                            jPanelDialog.add(jTextField);
                            jTextField.setHorizontalAlignment(SwingConstants.CENTER);
                            jDialog.add(jPanelDialog);
                            jDialog.setLocationRelativeTo(null);
                            jDialog.setResizable(false);
                            SettingsAndDecorView.getjFrameStartWindow().setEnabled(false);
                            jDialog.setVisible(true);
                            jDialog.setModal(true);
                            jDialog.addWindowListener(new WindowAdapter() {
                                public void windowClosing(WindowEvent e) {
                                    SettingsAndDecorView.getjFrameStartWindow().setEnabled(true);
                                    SettingsAndDecorView.getjFrameStartWindow().getContentPane().removeAll();
                                    SettingsAndDecorView.getjFrameStartWindow().getContentPane().invalidate();
                                    SettingsAndDecorView.getjFrameStartWindow().getContentPane().add
                                            (SettingsAndDecorView.getInstanceSettingsLoading().getjPanelStartWindow());
                                    SettingsAndDecorView.getjFrameStartWindow().getContentPane().revalidate();
                                    SettingsAndDecorView.getjFrameStartWindow().repaint();
                                }
                            });
                        }
                    }
                    if (size == 0) {
                        for (String s : stringArrayList) {
                            if (s.equals("\n") || s.equals(" ") || s.equals(",")) {
                                size++;
                            }
                        }
                    }
                    for (int i = 0; i < arrayAnsver.size(); i++) {
                        if (arrayAnsver.get(i).getName().equals(stringArrayList.get(i)) && arrayAnsver.get(i).getName().equals(e.getActionCommand())) {
                            size++;
                        }
                    }

                    if (size == stringArrayList.size()) {
                        JDialog jDialog = new JDialog();
                        jDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                        JPanel jPanelDialog = new JPanel();
                        jDialog.setSize(400, 400);
                        jTextField.setText("Вы выиграли");
                        jPanelDialog.add(jTextField);
                        jTextField.setHorizontalAlignment(SwingConstants.CENTER);
                        jDialog.add(jPanelDialog);
                        jDialog.setLocationRelativeTo(null);
                        jDialog.setResizable(false);
                        SettingsAndDecorView.getjFrameStartWindow().setEnabled(false);
                        jDialog.setVisible(true);
                        jDialog.setModal(true);
                        jDialog.addWindowListener(new WindowAdapter() {
                            public void windowClosing(WindowEvent e) {
                                SettingsAndDecorView.getjFrameStartWindow().setEnabled(true);
                                SettingsAndDecorView.getjFrameStartWindow().getContentPane().removeAll();
                                SettingsAndDecorView.getjFrameStartWindow().getContentPane().invalidate();
                                SettingsAndDecorView.getjFrameStartWindow().getContentPane().add
                                        (SettingsAndDecorView.getInstanceSettingsLoading().getjPanelStartWindow());
                                SettingsAndDecorView.getjFrameStartWindow().getContentPane().revalidate();
                                SettingsAndDecorView.getjFrameStartWindow().repaint();
                                jPanelInfoAndInput = null;
                                jPanelKeyBoard = null;
                                instance = null;
                            }
                        });
                    }
                    a.setEnabled(false);
                }
            });
        }

        jPanelInfoAndInput = new JPanel();


//        jPanelInfoAndInput.setBackground(Color.red);
        Dimension dimensionForOutPut = new Dimension();
        dimensionForOutPut.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                SettingsAndDecorView.getjFrameStartWindow().getHeight() / 2.5);
        jPanelInfoAndInput.setPreferredSize(dimensionForOutPut);

        JTextArea wordsForInput = new JTextArea();
        wordsForInput.setVisible(true);
        wordsForInput.setSelectedTextColor(Color.BLACK);
        wordsForInput.setEditable(true);
        wordsForInput.setBounds(850, 200, 300, 200);
        wordsForInput.setFont(new Font("Arial", Font.PLAIN, 18));
        jPanelInfoAndInput.setLayout(null);
        jPanelInfoAndInput.add(wordsForInput);

        JButton okey = new JButton();
        okey.setText("Already");
        okey.setBounds(1200, 200, 100, 100);
        jPanelInfoAndInput.add(okey);

        okey.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPanelInfoAndInput.remove(okey);
                jPanelInfoAndInput.remove(wordsForInput);
                jPanelInfoAndInput.repaint();
                char[] arrChar = wordsForInput.getText().toCharArray();
                for (char c : arrChar) {
                    stringArrayList.add(String.valueOf(c));
                }
                if (stringArrayList.size() >= 10 && stringArrayList.size() <= 19) {
                    missTake -= 2;
                }
                if (stringArrayList.size() > 20 && stringArrayList.size() <= 29) {
                    missTake -= 4;
                }
                if (stringArrayList.size() >= 30) {
                    missTake -= 6;
                }
                GridLayout forAnswer = new GridLayout(0, 12);
                forAnswer.setHgap(20);
                forAnswer.setVgap(20);
                if (stringArrayList.size() != 0) {
                    if (stringArrayList.size() > 4) {
                        dimensionForOutPut.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                                SettingsAndDecorView.getjFrameStartWindow().getHeight() / 8);
                        forAnswer.setColumns(forAnswer.getColumns() + 4);
                        if (stringArrayList.size() > 12) {
                            dimensionForOutPut.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                                    SettingsAndDecorView.getjFrameStartWindow().getHeight() / 6);
                            if (stringArrayList.size() > 26) {
                                forAnswer.setColumns(forAnswer.getColumns() + 5);
                                dimensionForOutPut.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                                        SettingsAndDecorView.getjFrameStartWindow().getHeight() / 4);
                                if (stringArrayList.size() > 36) {
                                    forAnswer.setColumns(forAnswer.getColumns() + 6);
                                    forAnswer.setHgap(10);
                                    forAnswer.setVgap(30);
                                    dimensionForOutPut.setSize(SettingsAndDecorView.getjFrameStartWindow().getWidth() / 3,
                                            SettingsAndDecorView.getjFrameStartWindow().getHeight() / 2.2);
                                }
                            }
                        }
                    }
                    arrayAnsver = new ArrayList<>(stringArrayList.size());
                    for (int i = 0; i < stringArrayList.size(); i++) {
                        if (stringArrayList.size() > 20) {
                            forAnswer.setColumns(20);
                        }
                        String name = stringArrayList.get(i);
                        arrayAnsver.add(new JButton());
                        arrayAnsver.get(i).setName(name);
                        if (stringArrayList.get(i).equals("\n") || stringArrayList.get(i).equals(" ") || stringArrayList.get(i).equals(",")) {
                            arrayAnsver.get(i).setName(String.valueOf(i));
                            arrayAnsver.get(i).setVisible(false);
                        }
                        int cheekble = 0;
                        if (i > 9 && stringArrayList.size() > 12 && stringArrayList.get(i).equals(" ")) {
                            cheekble = 1;
                        }
                        if (cheekble != 0) {
                            forAnswer.setRows(forAnswer.getRows() + 1);
                        }
                        arrayAnsver.get(i).setSize(jButtonExample.getWidth(), jButtonExample.getHeight());
                        arrayAnsver.get(i).setText("");
                        jPanelInfoAndInput.setLayout(forAnswer);
                        forAnswer.addLayoutComponent(arrayAnsver.get(i).getName(), arrayAnsver.get(i));
                        jPanelInfoAndInput.add(arrayAnsver.get(i));
                    }
                }
                jTextField.setText(String.valueOf(missTake));
                jPanelInfoAndInput.add(jTextField);
                jPanelInfoAndInput.updateUI();
            }
        });
    }

    @Override
    public boolean messageForOutPut(Boolean a, String name) {
        boolean rightAnswer = false;
        if (a) {
            for (JButton b : buttonArray) {
                if (b.getName().equals(name)) {
                    b.setBackground(Color.GREEN);
                    rightAnswer = true;
                    break;
                }
            }
        } else {
            for (JButton b : buttonArray) {
                if (b.getName().equals(name)) {
                    b.setBackground(Color.RED);
                    break;
                }
            }
        }
        return rightAnswer;
    }
}
