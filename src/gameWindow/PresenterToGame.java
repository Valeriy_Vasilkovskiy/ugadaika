package gameWindow;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class PresenterToGame implements IPresenterTogame {
    private ModelToGame modelToGame;
    private IGameview iGameview;

    PresenterToGame(IGameview iGameview) {
        this.modelToGame = new ModelToGame();
        this.iGameview = iGameview;
    }

    @Override
    public boolean NumberCheek(ArrayList<String> array, ActionEvent e, String name) {
        return iGameview.messageForOutPut(modelToGame.cheeckPressRight(array, e), name);
    }
}
