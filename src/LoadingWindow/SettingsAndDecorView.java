package LoadingWindow;

import gameWindow.GameViewPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class SettingsAndDecorView implements Iview {
    private static JFrame jFrameStartWindow;
    private static SettingsAndDecorView instance;

    public static SettingsAndDecorView getInstanceSettingsLoading() {
        if (instance == null) {
            instance = new SettingsAndDecorView();
        }
        return instance;
    }

    public JPanel getjPanelStartWindow() {
        return jPanelStartWindow;
    }

    public static JFrame getjFrameStartWindow() {
        return jFrameStartWindow;
    }

    private JPanel jPanelStartWindow;
    private IPresenter iPresenter;

    private SettingsAndDecorView() {
        iPresenter = new PresenterForLoadingWindow(this);
        jFrameStartWindow = new JFrame();
        jPanelStartWindow = new JPanel();
        jFrameStartWindow.getContentPane().add(jPanelStartWindow);
        jPanelStartWindow.setLayout(null);
        jFrameStartWindow.setSize(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
        jFrameStartWindow.setLocationRelativeTo(null);
        jFrameStartWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrameStartWindow.revalidate();
        jFrameStartWindow.setVisible(true);


        JButton start = new JButton("Start");
        start.setSize(jFrameStartWindow.getWidth() / 10, jFrameStartWindow.getHeight() / 13);
        start.setLocation(jFrameStartWindow.getWidth() / 2 - start.getWidth() / 2, jFrameStartWindow.getHeight() / 2 - 125);
        jPanelStartWindow.add(start);

        JButton exit = new JButton("Exit");
        exit.setSize(jFrameStartWindow.getWidth() / 10, jFrameStartWindow.getHeight() / 13);
        exit.setLocation(jFrameStartWindow.getWidth() / 2 - exit.getWidth() / 2, jFrameStartWindow.getHeight() / 2);
        jPanelStartWindow.add(exit);


        start.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameStartWindow.getContentPane().removeAll();
                jFrameStartWindow.getContentPane().invalidate();
                jFrameStartWindow.getContentPane().add(GameViewPanel.getInstanceGame().getjPanelKeyBoard(), BorderLayout.SOUTH);
                jFrameStartWindow.getContentPane().add(GameViewPanel.getInstanceGame().getjPanelInfoAndInput(), BorderLayout.NORTH);
                jFrameStartWindow.getContentPane().revalidate();
                jFrameStartWindow.repaint();
            }
        });
        exit.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameStartWindow.dispose();
            }
        });
    }

}
